General information: 
This project was about creating a robot that could hit the drums using given commands. 
We made separate programs for the robot and the PC, the robot receives the commands via bluetooth, and then performs them. 
In this repository we have the robot's program in the first file, and the rest is for the PC remote controller.


Requirements: 
You need the LeJOS API for eclipse.


Contact information: 
Group 10: Otso, Lauri, Tiia. 
Metropolia University of applied sciences,
Information and communications technology. 


Notices: 
Not sure if the JDOC has been committed. Also the programs might not work with every kind of a drumming robot.