package rumpaliController;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import rumpaliModel.PCtoRobotModel;

/**
 * Luokka <code>MainScreenController</code> toimii
 * <code>rumpaliMainScreen</code>in ohjaajana.
 *
 * @author Otso,Tiia,Lauri
 * @version 1.0
 */

public class MainScreenController {

	/**
	 * <code>demoButton</code> k�ynnist�� robotissa demon soittamisen.
	 */
	@FXML
	private Button demoButton;
	/**
	 * <code>connectButton</code> yhdist�� robottiin.
	 */
	@FXML
	public Button connectButton;
	/**
	 * <code>startButton</code> k�ynnist�� robotissa omavalintaisen rummutuksen.
	 */
	@FXML
	private Button startButton;
	/**
	 * <code>steadyButton</code> lopettaa rummutuksen.
	 */
	@FXML
	private Button steadyButton;
	/**
	 * <code>stopButton</code> lopettaa robotin toiminnan.
	 */
	@FXML
	private Button stopButton;

	/**
	 * <code>checkBoxTextField</code>iin voi antaa halutun CheckBoxien m��r�n.
	 */
	@FXML
	private TextField checkBoxTextField;

	/**
	 * <code>checkBoxContainer</code> sis�lt�� kaikki CheckBoxit.
	 */
	@FXML
	private GridPane checkBoxContainer = new GridPane();
	/**
	 * <code>topLeftButtonsFP</code> sis�t�� kaikki vasempaan yl�kulmaan tulevat
	 * elementit.
	 */
	@FXML
	private FlowPane topLeftFlowPane;
	/**
	 * <code>topRightButtonsGrid</code> sis�lt�� oikeaan yl�kumlmaan tulevat
	 * elementit.
	 */
	@FXML
	private GridPane topRightGrid;
	/**
	 * <code>checkBoxOptionsDescription</code> kuvailee CheckBoxien asetuksia.
	 */
	@FXML
	private Text checkBoxOptionsDescription;

	/**
	 * <code>checkBoxes</code> sis�lt�� kaikki halutut CheckBoxit.
	 */
	private List<CheckBox> checkBoxes;
	/**
	 * <code>PCtoRobot</code> luo <code>PCtoRobotModel</code> olion, joka
	 * huolehtii yhteydest� robottiin.
	 */
	private PCtoRobotModel PCtoRobot = new PCtoRobotModel(this);
	/**
	 * <code>checkBoxSlider</code> s��t�� haluttujen CheckBoxien m��r��.
	 */
	@FXML
	private Slider checkBoxSlider;

	/**
	 * <code>startButtonPressed</code> k�skee <code>PCtoRobot</code>tia
	 * vaihtamaan kappaletta haluttuihin CheckBoxeihin
	 * <code>startButton</code>ia painettaessa.
	 */
	@FXML
	public void startButtonPressed(ActionEvent event) {
		PCtoRobot.newSong(checkBoxes);

	}

	/**
	 * <code>connectButtonPressed</code> k�skee <code>PCtoRobot</code>tia
	 * yhdist�m��n robottiin <code>connectButton</code>ia painettaessa.
	 */

	@FXML
	public void connectButtonPressed(ActionEvent event) {

		disableElements(!PCtoRobot.connect());

	}

	/**
	 * <code>steadyButtonPressed</code> k�skee <code>PCtoRobot</code>tia
	 * lopettamaan soiton <code>steadyButton</code>ia painettaessa.
	 */
	@FXML
	public void steadyButtonPressed(ActionEvent event) {
		PCtoRobot.steady();

	}

	/**
	 * <code>checkBoxTextFieldOnEnter</code> k�ytt��
	 * <code>setSize</code>-metodia asettamaan CheckBoxien lukum��r�n enteri�
	 * painettaessa, kun <code>checkBoxTextField</code> on aktiivinen.
	 *
	 * T�ss� hy�dynnet��n <code>PCtoRobot</code>in omaa setSize metodia oikean
	 * lukum��r�n laskemiseen k�ytt�j�n sy�t�n perusteella.
	 */

	@FXML
	public void checkBoxTextFieldOnEnter(ActionEvent ae) {

		int size = PCtoRobot.setSize(Integer.parseInt(checkBoxTextField.getText()));
		checkBoxSlider.setValue(size);

		setSize(size);

	}

	/**
	 * <code>stopButtonPressed</code> k�skee <code>PCtoRobot</code>tia
	 * pys�ytt�m��n toiminnan <code>stopButton</code>ia painettaessa.
	 */
	@FXML
	public void stopButtonPressed(ActionEvent event) {
		PCtoRobot.stop();

	}

	/**
	 * <code>demoButtonPressed</code> k�skee <code>PCtoRobot</code>tia
	 * k�ynnist�m��n demonstraation <code>demoButton</code>ia painettaessa.
	 */
	@FXML
	public void demoButtonPressed(ActionEvent event) {
		PCtoRobot.demo();
	}

	/**
	 * <code>setSize</code> asettaa <code>checkBoxes</code> listan CheckBoxien
	 * m��r�n vastaamaan annettua m��r��
	 *
	 */
	public void setSize(int size) {
		checkBoxes = new ArrayList<>();

		checkBoxes.clear();
		checkBoxContainer.getChildren().clear();
		checkBoxes = new ArrayList<>();

		for (int i = 1; i <= size; i++) {
			CheckBox cb = new CheckBox();
			cb.setId("cb" + 0 + i);
			checkBoxes.add(cb);
			checkBoxContainer.add(cb, i, 0);
		}

		for (int i = 1; i <= size; i++) {
			CheckBox cb = new CheckBox();
			cb.setId("cb" + 1 + i);
			checkBoxes.add(cb);
			checkBoxContainer.add(cb, i, 1);
		}
	}

	/**
	 * <code>disableElements</code> lukitsee kaikkien panejen elementit sen
	 * mukaan, onko yhteytt�.
	 *
	 */
	public void disableElements(boolean isConnected) {

		for (Node children : topLeftFlowPane.getChildren()) {
			children.setDisable(isConnected);
		}

		for (Node children : topRightGrid.getChildren()) {
			children.setDisable(isConnected);
		}

		for (Node children : checkBoxContainer.getChildren()) {
			children.setDisable(isConnected);
		}

		connectButton.setDisable(!isConnected);

	}

	/**
	 * <code>isDisabled</code> kertoo onko panejen elementit lukittu.
	 *
	 */
	public boolean isDisabled() {

		return startButton.isDisabled();

	}

	/**
	 * <code>initialize</code> alustaa FXML elementit ja metodit, sek� asettaa
	 * kuuntelijat.
	 *
	 */
	public void initialize() {

		setSize(20);
		checkBoxOptionsDescription.setText("Adjusts the amount \nof boxes");

		checkBoxSlider.valueProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
				setSize((int) checkBoxSlider.getValue());

			}
		});
		disableElements(true);
	}
}
