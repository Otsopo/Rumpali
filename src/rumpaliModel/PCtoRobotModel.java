package rumpaliModel;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.CheckBox;
import lejos.utility.Delay;
import rumpaliController.MainScreenController;

/**
 * Luokka <code>PCtoRobotModel</code> toimii <code>MainScreenController</code>in
 * mallina ja huolehtii muun muassa yhteydest�
 * robottiin.<code>PCtoRobotModel</code> toteuttaa
 * <code>PCtoRobotModel_IF</code> rajapintaa.
 *
 * @author Otso,Tiia,Lauri
 * @version 1.0
 */
public class PCtoRobotModel implements PCtoRobotModel_IF {

	MainScreenController mainScreenController = null;
	ConnectionThread connectionChecker = null;
	/**
	 * Lista <code>binaryCheckBoxes</code> sis�lt�� k�ytettyjen CheckBoxien
	 * valintatiedot bin��rimuodossa.
	 *
	 *
	 */
	private List<Integer> binaryCheckBoxes = new ArrayList<>();
	/**
	 * Lista <code>intCheckBoxes</code> sis�lt�� k�ytettyjen CheckBoxien
	 * valintatiedot Integer-muodossa.
	 *
	 *
	 */

	private List<Integer> intCheckBoxes = new ArrayList<>();

	/**
	 * <code>dataSocket</code> on int lukujen l�hett�miseen tarkoitettu
	 * <code>Socket</code>.
	 */
	Socket dataSocket;
	/**
	 * <code>objectSocket</code> on <code>intCheckBoxes</code> -listan
	 * l�hett�miseen tarkoitettu <code>Socket</code>.
	 */
	Socket objectSocket;
	/**
	 * <code>dataOut</code> on int lukujen l�hett�miseen tarkoitettu
	 * <code>DataOutputStream</code>.
	 */
	DataOutputStream dataOut;
	/**
	 * <code>objectOut</code> on <code>intCheckBoxes</code> -listan
	 * l�hett�miseen tarkoitettu <code>ObjectOutputStream</code>.
	 */
	ObjectOutputStream objectOut;

	/**
	 * Konstruktori joka alustaa <code>MainScreenController</code>in ja <code>ConnectionThread</code>in.
	 */
	public PCtoRobotModel(MainScreenController msc) {
		connectionChecker = new ConnectionThread(this, msc);
		this.mainScreenController = msc;
	}

	/**
	 * <code>newSong</code> muuttaa annetun CheckBox -listan robotille
	 * luettavaan muotoon ja antaa sen flushattavaksi. Ensin flushataan numero
	 * 1, jonka vastaanotettuaan robotti tiet�� odottaa flushattavaa listaa
	 * listaa.
	 *
	 * @param checkBoxes
	 *            muutetaan ensin bin��ri ja sitten int muotoon sen perusteella,
	 *            mitk� CheckBoxeista on valittu.
	 */

	@Override
	public void newSong(List<CheckBox> checkBoxes) {

		for (CheckBox cb : checkBoxes) {
			int intcb = (cb.isSelected()) ? 1 : 0;
			binaryCheckBoxes.add(intcb);

		}

		for (int i = 0; i < binaryCheckBoxes.size() / 2; i++) {
			if (binaryCheckBoxes.get(i) == 0 && binaryCheckBoxes.get(i + binaryCheckBoxes.size() / 2) == 0) {
				intCheckBoxes.add(0);
			} else if (binaryCheckBoxes.get(i) == 1 && binaryCheckBoxes.get(i + binaryCheckBoxes.size() / 2) == 0) {
				intCheckBoxes.add(1);
			} else if (binaryCheckBoxes.get(i) == 0 && binaryCheckBoxes.get(i + binaryCheckBoxes.size() / 2) == 1) {
				intCheckBoxes.add(2);
			} else if (binaryCheckBoxes.get(i) == 1 && binaryCheckBoxes.get(i + binaryCheckBoxes.size() / 2) == 1) {
				intCheckBoxes.add(3);
			}

		}

		flushList(intCheckBoxes);
		Delay.msDelay(1000);
		flushInt(1);


	}

	/**
	 * Antaa demo-k�skyn flushattavaksi.
	 */
	@Override
	public void demo() {
		flushInt(2);

	}

	/**
	 * Antaa stop-k�skyn flushattavaksi.
	 */
	@Override
	public void stop() {
		flushInt(0);

	}

	/**
	 * Antaa steady-k�skyn flushattavaksi.
	 */
	@Override
	public void steady() {
		flushInt(3);

	}

	/**
	 * Flushaa annetun luvun.
	 *
	 * @param command
	 *            on luku, joka robotti ymm�rt�� k�skyn�.
	 * @return true kun flushaaminen onnistuu.
	 * @return false jos flushaaminen ep�onnistuu.
	 */
	@Override
	public boolean flushInt(int command) {
		try {

			dataOut.writeInt(command);
			dataOut.flush();
			System.out.println(command + " Flushed");
			return true;
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * Flushaa annetun listan.
	 *
	 * @param intCheckBoxes
	 *            sis�lt�� tiedon soittokomennoista robotille selv�ss� muodossa.
	 * @return true kun flushaaminen onnistuu.
	 * @return false jos flushaaminen ep�onnistuu.
	 */
	@Override
	public boolean flushList(List<Integer> intCheckBoxes) {
		System.out.println(intCheckBoxes.toString());

		try {
			objectOut.reset();
			objectOut.writeObject(intCheckBoxes);
			objectOut.flush();
			binaryCheckBoxes.clear();
			intCheckBoxes.clear();
			return true;

		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}



	/**
	 * Asettaa ja muuntaa annetun CheckBox m��r�n.
	 *
	 * @param size
	 *            on k�ytt�j�n antama m��r�.
	 * @return size palautetaan ohjaimelle.
	 */
	@Override
	public int setSize(int size) {
		if (size > 20) {
			size = 20;
		} else if (size < 1) {
			size = 1;
		}

		return size;
	}

	/**
	 * Alustaa socketit ja virrat.
	 *
	 * @return true jos yhteys muodostetaan.
	 * @return false jos yhteytt� ei muodosteta.
	 */
	@Override
	public boolean connect() {
		try {
			dataSocket = new Socket("10.0.1.1", 1111);
			objectSocket = new Socket("10.0.1.1", 1112);
			dataOut = new DataOutputStream(dataSocket.getOutputStream());
			objectOut = new ObjectOutputStream(objectSocket.getOutputStream());
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {


			runConnectionThread();
		}

	}

	/**
	 * Tarkistaa onko yhteytt� robottiin l�hett�m�ll� sille numeroa 10.
	 *
	 * @return true jos on yhteys.
	 */
	@Override
	public boolean isConnected() {
		if (flushInt(10)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * K�ynnist�� s�ikeen joka tarkkailee yhteytt� robottiin.
	 *
	 */
	public void runConnectionThread() {
		connectionChecker = new ConnectionThread(this, mainScreenController);
		connectionChecker.start();
	}

}
