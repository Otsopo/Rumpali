package rumpaliModel;

import java.util.List;

import javafx.scene.control.CheckBox;

public interface PCtoRobotModel_IF {

abstract public void demo();
abstract public void newSong(List<CheckBox> RCBoxes);
abstract public void stop();
abstract public boolean flushInt(int command);
abstract public boolean flushList(List<Integer> intRCBoxes);
abstract public int setSize(int size);
abstract public void steady();
abstract public boolean connect();
abstract public boolean isConnected();



}
