package rumpaliModel;


import rumpaliController.MainScreenController;

/**
 * Luokka <code>ConnectionThread</code> on s�ie,
 * joka tarkkailee yhteytt� robottiin ja huolehtii k�ytt�liittym�n lukitsemisesta.
 *
 * @author Otso,Tiia,Lauri
 * @version 1.0
 */
public class ConnectionThread extends Thread {
	MainScreenController msc = null;
	PCtoRobotModel model = null;
	/**
	 * <code>done</code> kertoo onko robotti valmis.
	 */
	boolean done = false;
	/**
	 * <code>connected</code> kertoo onko yhteytt�.
	 */
	boolean connected = false;
	/**
	 * <code>disabled</code> kertoo onko k�ytt�liittym� lukittu.
	 */
	boolean disabled = true;


	public ConnectionThread(PCtoRobotModel model, MainScreenController msc) {
		this.model = model;
		this.msc = msc;
	}


	/**
	 * <code>run</code> metodi tarkkailee viiden sekunnin v�lein onko robottiin yhteytt� tai k�ytt�liittym� lukittu.
	 * Se reagoi aina tilanteeseen sen vaatimalla tavalla.
	 */
	public void run() {
		while (done == false) {
			try{
			connected = model.isConnected();
			disabled = msc.isDisabled();

			if (connected == false && disabled == true) {
				done();


			}
			else if (connected == true && disabled == false) {



			}

			else if (connected == false && disabled == false) {
				msc.disableElements(true);
				msc.connectButton.setDisable(false);
				done();


			}


			else if (connected == true && disabled == true) {
				msc.disableElements(false);
				msc.connectButton.setDisable(true);

			}
			}
			catch(NullPointerException e){
				msc.disableElements(true);
				msc.connectButton.setDisable(false);
				done();

			}
			catch(Exception e){
				msc.disableElements(true);
				msc.connectButton.setDisable(false);
				done();


			}


			try {
				sleep(5000);
			} catch (InterruptedException ie) {
				System.err.println("Sleep interrupted");
			}

		}


	}

	/**
	 * <code>done</code>  lopettaa s�ikeen toiminnan.
	 */
	public void done() {
		done = true;
	}

}
