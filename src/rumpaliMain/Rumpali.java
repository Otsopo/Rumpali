package rumpaliMain;

import javafx.application.Application;

/**
 * Luokka <code>Rumpali</code> k�ynnist��
 * <code>rumpaliMainScreen</code>in <code>start</code> metodissa.
 *
 * @author Otso,Tiia,Lauri
 * @version 1.0
 */
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Luokka <code>Rumpali</code> k�ynnist�� JavaFX s�ikeen.
 *
 * @author Otso,Tiia,Lauri
 * @version 1.0
 */
public class Rumpali extends Application {

	/**
	 * <code>start</code> asettaa Stagen ja Scenen.
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/rumpaliGUI/rumpaliMainScreen.fxml"));

			Scene scene = new Scene(root);
			primaryStage.setTitle("Rumpali");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * T�ss� k�ynnistet��n itse s�ie.
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
