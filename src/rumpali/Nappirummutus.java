package rumpali;

import lejos.robotics.subsumption.Behavior;
/**
 * <code>Nappirummutus</code> behavior on kayttaytyminen jossa robotti kuuntelee nappeja ja liikuttaa moottoreita kayttajan painallusten mukaan.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */
public class Nappirummutus implements Behavior {

	/**
	 * muuttuja <code>action</code> metodin lopettamiseen.
	 */
	private volatile boolean suppressed = false;

	/**
	 * olio kayttaa moottoreita.
	 */
	private Moottorit moottorit;
	/**
	 * olio kayttaa nappeja.
	 */
	private Nappi nappi;
	/**
	 * luodaan rummutussaikeet.
	 */
	NappirummutussaieL saieL;
	NappirummutussaieR saieR;

	/**
	 * Luokan konstruktori joka asettaa moottorit ja nappi oliot.
	 * @param moottorit
	 * @param nappi
	 */
	public Nappirummutus(Moottorit moottorit, Nappi nappi) {
		this.moottorit = moottorit;
		this.nappi = nappi;



	}

	/**
	 * Metodi <code>take control</code> ilmaisee milloin kayttaytyminen ottaa hallinnan.
	 */
	@Override
	public boolean takeControl() {
		return true;
	}

	/**
	 * <code>action</code> on kayttaytymisen toiminta metodi jota se kay kun kayttaytymisella on kontrolli.
	 */
	@Override
	public void action(){
		suppressed = false;
		saieL = new NappirummutussaieL(nappi, moottorit);
		saieR = new NappirummutussaieR(nappi, moottorit);
		saieL.start();
		saieR.start();
		while (!suppressed){

		}
		saieL.lopeta();
		saieR.lopeta();
	}

	/**
	 * Kayttaytymisen lopettaja metodi <code>suppress</code>.
	 */
	@Override
	public void suppress() {
		suppressed = true;
	}

}
