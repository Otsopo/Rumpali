
package rumpali;
import lejos.robotics.subsumption.Behavior;
/**
 * <code>Stop</code> on robotin stop behavior, joka pysayttaa robotin toiminnan.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */

public class Stop implements Behavior {

	private volatile boolean suppressed = false;
	private Sailio sailio;

	/**
	 * Luokan konstruktori joka saa parametreinaan <code>Sailio</code>-olion.
	 * @param sailio
	 */
	public Stop(Sailio sailio){
		this.sailio = sailio;
	}

	/**
	 * <code>take control</code> kaynnistaa kayttaymisen palauttamalla true kun <code>Sailio</code> saa arvon 0.
	 */
	@Override
	public boolean takeControl() {
		if(sailio.getLuku()==0) {
			return true;
		}
		return false;
	}

	/**
	 * Luokan kayttaytyminen <code>action</code> joka sulkee koko ohjelman.
	 */
	@Override
	public void action() {
		System.exit(0);

	}
	/**
	 *Metodi <code>suppress</code> lopettaa <code>action</code> metodin.
	 */
	@Override
	public void suppress() {
		suppressed = true;
	}


}
