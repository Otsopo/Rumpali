
package rumpali;


import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;


//lukee arvon ja tallettaa sen.
/**
 * Luokka <code>Kasittelijasaie</code> sisaltaa datavirran kasittelijan.
 *
 * @author Lauri, Tiia, Otso
 *@version 1.0
 */
public class Kasittelijasaie extends Thread {

	/**
	 * Luokan kayttama sailio.
	 */

	private Sailio sailio;
	/**
	 * Luokan kayttama soketti.
	 */
	private Socket s;
	/**
	 * Datavirrasta talteen otettava integeri.
	 */
	private int luku;
	/**
	 * Luo <code>Kasittelijasaie</code> olion ilmentyman, jonka sailioksi tulee <code>sailio</code> ja soketiksi <code>s</code>.
	 * @param s
	 * 			ilmentyman soketti
	 * @param sailio
	 * 			ilmentyman sailio
	 */


	public Kasittelijasaie(Socket s, Sailio sailio) {
		this.s = s;
		this.sailio = sailio;
	}
	/**
	 * Saikeen kaynnistyessa ajettava metodi. Luo datavirran ja vastaanottaa integereita ja tallettaa ne sailioon.
	 */


	@Override
	public void run() {



		try {

			/**
			 * <code>in</code> on DataInputStreami joka vastaanottaa tietoa pc:ltä.
			 */

			DataInputStream in = new DataInputStream(s.getInputStream());

			while(true){

				luku = in.readInt();
				sailio.setLuku(luku);

				if (luku == 0){
					in.close();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();

		}

	}
}
