
package rumpali;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.NXTRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.utility.Delay;

/**
 * <code>Moottorit</code> luokka, jossa maaritetaan moottorit ja niiden toimintametodit.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */

public class Moottorit {

	/**
	 * <code>motorRightPort</code> ja <code>motorLeftPort</code> ovat porttiolion ilmentymia joihin annetaan parametrina robotin portti.
	 */
    private Port motorRightPort = LocalEV3.get().getPort("A");
    private Port motorLeftPort = LocalEV3.get().getPort("B");

    /**
     * <code>rightMotor</code> ja <code>leftMotor</code> ovat NXTRegulatedMotor luokan ilmentymia jotka saavat parametrina portit joihin moorrotir on kytketty.
     */

    private NXTRegulatedMotor rightMotor = new NXTRegulatedMotor(motorRightPort);
    private NXTRegulatedMotor leftMotor = new NXTRegulatedMotor(motorLeftPort);

    /**
     * <code>alkuAsento</code> maarittaa robotin rumpukapuloiden asennon johon ne palaa iskun jalkeen.
     */

    private int alkuAsento = 0;

    /**
     * <code>delay</code> maarittaa iskuun kuluneen ajan.
     */
    private int delay = 60;

    public Moottorit() {

    }

    /**
     * <code>molemmat()</code> metodi liikuttaa molempia moottoreita yhta-aikaa ja palauttaa iskun jalkeen molemmat alkuasentoon.
     */

    public void molemmat() {
    	rightMotor.backward();
    	leftMotor.backward();
    	Delay.msDelay(delay);
    	rightMotor.rotateTo(alkuAsento, true);
    	leftMotor.rotateTo(alkuAsento);


    }

    /**
     * <code>oikea()</code> metodi liikuttaa oikeanpuoleista moottoria ja palauttaa iskun jalkeen takaisin alkuasentoon.
     */
    public void oikea() {

    	rightMotor.backward();
    	Delay.msDelay(delay);
    	rightMotor.rotateTo(alkuAsento);
    }

    /**
     * <code>vasen()</code> metodi liikuttaa vasemmanpuoleista moottoria ja palauttaa iskun jalkeen takisin alkuasentoon.
     */
    public void vasen() {

    	leftMotor.backward();
    	Delay.msDelay(delay);
    	leftMotor.rotateTo(alkuAsento);
    }

    /**
     * <code>tyhja()</code> metodi odottaa yhden lyonnin verran.
     */
    public void tyhja() {
    	Delay.msDelay(delay*2);
    }

    /**
     * <code>seis()</code> metodi pysayttaa molemmat moottorit.
     */
    public void seis() {
    	rightMotor.stop();
    	leftMotor.stop();
    }
}
