
package rumpali;

import java.util.ArrayList;

import lejos.robotics.subsumption.Behavior;
/**
 * Behavior <code>Rummutus</code> liikuttaa moottoreita <code>Sailio</code> olion <code>int lista</code> listan mukaan.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */
public class Rummutus implements Behavior {

	private volatile boolean suppressed = false;

	private Moottorit moottorit;
	private Sailio sailio;
	private ArrayList<Integer> lista;

	/**
	 * <code>lista</code> listan setteri.
	 * @param lista
	 */
	public void setLista(ArrayList<Integer> lista) {
		this.lista = lista;
	}

	/**
	 * Luokan konstruktori joka saa <code>Moottorit</code> ja <code>Sailio</code> oliot.
	 * @param moottorit
	 * @param sailio
	 */
	public Rummutus(Moottorit moottorit, Sailio sailio) {
		this.sailio = sailio;
		this.moottorit = moottorit;
	}

	/**
	 * Luokan haltuunotto metodi joka palauttaa <code>true</code> kun saa <code>Sailio</code> oliolta arvon 1.
	 */
	@Override
	public boolean takeControl() {
		if(sailio.getLuku() == 1) {
			return true;
		}
		return false;
	}

	/**
	 * Luokan kayttaytyminen joka liikuttaa moottoreita <code>lista</code> listan mukaan.
	 */
	@Override
	public void action() {
		suppressed = false;
		do{
			while (sailio.getLista() == null);
			lista = sailio.getLista();


			for(int i : lista) {
				if(sailio.getLuku()==3){
					suppress();
				}
				if(suppressed) {
					break;
				}
				if (i==1) {
					moottorit.oikea();
				} else if (i==2) {
					moottorit.vasen();
				} else if (i==3) {
					moottorit.molemmat();
				} else {
					moottorit.tyhja();
				}
			}
		}while(!suppressed);

	}

	/**
	 * Lopettaa <code>action()</code> metodin.
	 */
	@Override
	public void suppress() {
		suppressed = true;

	}


}
