
package rumpali;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

/**
 * Luokka <code>Main</code>, joka kaynnistaa ohjelman ja maaraa kaynnistymisessa tehtavat toimet.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */
public class Main {

	/**
	 * <Code>Main</Code> metodi kaynnistaa ohjelman. Siina luodaan yhteysaie ja kaynnistetaan se.
	 * Metodissa myos luodaan ja kaynnistetaan Arbitrator, johon talletetaan kayttaytymiset.
	 * @throws InterruptedException
	 *
	 */

	public static void main(String[] args) {

		/**
		 * <code>sailio</code> on ohjelman kayttama sailio olion ilmentyma.
		 */

		Sailio sailio = new Sailio();

		/**
		 * <code>yhteyssaie</code> on yhteyssaie olion ilmentyma.
		 */
		Yhteyssaie yhteyssaie = new Yhteyssaie(sailio);
		yhteyssaie.start();

		/**
		 *<code>moottorit</code> on ohjelman kayttama moottorit olion ilmentyma.
		 */

		Moottorit moottorit = new Moottorit();
		/**
		 * <code>nappi</code> on ohjelman kayttama nappi olion ilmentyma.
		 */
		Nappi nappi = new Nappi();
		/**
		 * <code>b1</code> on Nappirummutus luokan Behavior.
		 */
		Behavior b1 = new Nappirummutus(moottorit, nappi);
		/**
		 * <code>b2</code> on Demo luokan Behavior.
		 */
		Behavior b2 = new Demo(moottorit, sailio);
		/**
		 * <code>b3</code> on Rummutus luokan Behavior.
		 */
		Behavior b3 = new Rummutus(moottorit, sailio);
		/**
		 * <code>b4</code> on Stop luokan Behavior.
		 */
		Behavior b4 = new Stop(sailio);
		/**
		 * <code>bList</code> on lista, johon behaviorit talletetaan.
		 */

		Behavior[] bList = {b1,b2,b3,b4};
		/**
		 * <code>arbi</code> on Arbitrator joka saatelee Behavioreiden toimintaa ja prioriteettijarjestysta.
		 */
		Arbitrator arbi = new Arbitrator(bList);
		arbi.go();

	}

}
