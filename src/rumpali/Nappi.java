package rumpali;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.SampleProvider;

/**
 * <code>Nappi</code> luokka, jossa maaritetaan napit jotka paluttaa true/false kun on painettu/ei painettu.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */

public class Nappi {
	/**
	 * <code>portLeft</code> ja <code>portRight</code> ovat porttiolion ilmentymia joihin annetaan parametrina robotin portti.
	 */
	Port portLeft = LocalEV3.get().getPort("S3");
	Port portRight = LocalEV3.get().getPort("S2");

	/**
	 * <code>sensorL</code> ja <code>sensorR</code> ovat EV3TouchSensor luokan ilmentymia jotka saavat parametrina portit joihin napit on kytketty.
	 */
	SensorModes sensorL = new EV3TouchSensor(portLeft);
	SensorModes sensorR = new EV3TouchSensor(portRight);
	/**
	 * <code>touchL</code> ja <code>touchR</code> keraavat naytteet napeista.
	 */
	SampleProvider touchL = ((EV3TouchSensor)sensorL).getTouchMode();
	SampleProvider touchR = ((EV3TouchSensor)sensorR).getTouchMode();
	/**
	 * <code>sampleL</code> ja <code>sampleR</code> ovat listoja joihin tallennetaan otetut naytteet.
	 */
	float[] sampleL = new float[touchL.sampleSize()];
	float[] sampleR = new float[touchR.sampleSize()];


	public Nappi(){

	}

	/**
	 * <code>getLeft</code> ottaa naytteen vasemmasta napista ja palauttaa true/false kun nappi on painettu/ei painettu.
	 * @return palauttaa tiedon onko nappi painettu
	 */
	public boolean getLeft(){
		touchL.fetchSample(sampleL, 0);
		if (sampleL[0] > 0.5){
			return true;
		}
		else{
			return false;
		}
	}

	/**
	 * <code>getLeft</code> ottaa naytteen oikeasta napista ja palauttaa true/false kun nappi on painettu/ei painettu.
	 * @return palauttaa tiedon onko nappi painettu
	 */
	public boolean getRight(){
		touchR.fetchSample(sampleR, 0);
		if (sampleR[0] > 0.5){
			return true;
		}
		else{
			return false;
		}
	}


}
