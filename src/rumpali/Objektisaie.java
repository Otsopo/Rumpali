
package rumpali;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 * <code>Objektisaie</code> luokka maarittelee objektisaikeen toiminnan.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */

public class Objektisaie extends Thread {

	/**
	 * <code>sailio</code> on saikeen kayttama Sailio olion ilmentyma.
	 */

	private Sailio sailio;

	/**
	 * <code>s</code> on saikeen kayttama Socket olion ilmentyma.
	 */
	private Socket s;

	/**
	 * Olion konstruktori. Se saa parametreinaan <code>sailio</code> olion ja <code>s</code> nimisen Socket olion.
	 * @param sailio
	 * @param s
	 */

	public Objektisaie(Sailio sailio, Socket s) {
		this.sailio = sailio;
		this.s = s;
	}

	/**
	 * <code>run</code> metodi maarittaa mita tapahtuu kun saie kaynnistetaan. Siina luodaan objektistreami
	 * ja arraylista johon talletetaan streamista tuleva lista. Kyseinen lista viedaan sailioon.
	 */

	public void run() {

		try {
			/**
			 * <code>lista</code> on arraylista johon talletetaan streamista tulevat listat.
			 */
			ArrayList<Integer> lista;
			/**
			 * <code>oin</code> on ObjektInputStreamin ilmentyma.
			 */
			ObjectInputStream oin = new ObjectInputStream(s.getInputStream());
			while(true){
				lista = (ArrayList<Integer>) oin.readObject();
				sailio.setLista(lista);
				yield();
			}


		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



}
