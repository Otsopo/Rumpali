package rumpali;
/**
 * <code>NappirummutussaieL</code> saie, joka kuuntelee oikeanpuoleista nappia ja pyorittaa oikeanpuoleista moottoria.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */
public class NappirummutussaieR extends Thread {
	private Nappi nappi;
	private Moottorit moottorit;
	private volatile boolean valmis = false;

	/**
	 * luokan konstruktori joka asettaa napin ja moottorin.
	 * @param nappi
	 * @param moottorit
	 */
	public NappirummutussaieR(Nappi nappi, Moottorit moottorit){
		this.nappi = nappi;
		this.moottorit = moottorit;
	}

	/**
	 * kun saie kaynnistetaan <code>run</code> metodilla se kuuntelee oikeanpuoleista nappia. Kun nappi on painettu, saie pyorittaa oikeanpuoleista moottoria.
	 */
	@Override
	public void run(){
		while (!valmis){
			while (nappi.getRight()){
				moottorit.oikea();
			}
		}

	}
	/**
	 * saikeen lopetus metodi <code>lopeta</code>.
	 */
	public void lopeta(){
		valmis = true;
	}



}


