
package rumpali;

import java.util.ArrayList;
/**
 * Luokkaan <code>Sailio</code> talletetaan kasittelija- ja objektisaikeelta tulleet arvot
 * josta muut oliot kayvat arvot lukemassa.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */

public class Sailio {

	private ArrayList<Integer> lista = null;
	private int luku = 10;

	/**
	 * <code>getLista()</code> listan getteri.
	 * @return palauttaa listan
	 */
	public ArrayList<Integer> getLista() {
		return lista;
	}

	/**
	 * <code>setLista()</code> listan setteri.
	 * @param lista
	 */
	public void setLista(ArrayList<Integer> lista) {
		this.lista = lista;
		System.out.println("Lista viety ");

	}

	/**
	 * <code>int luku</code> muuttujan getteri.
	 * @return palauttaa luvun
	 */
	public int getLuku() {
		return luku;
	}

	/**
	 * <code>int luku</code> muuttujan setteri.
	 *
	 */
	public void setLuku(int luku) {
		this.luku = luku;

	}


}
