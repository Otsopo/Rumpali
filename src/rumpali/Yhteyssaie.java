
package rumpali;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
/**
 * <code>Yhteyssaie</code> luokka luo soketin ja kasittelijasaikeen ja objektisaikeen.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */

public class Yhteyssaie extends Thread {

	private Sailio sailio;

	/**
	 * Luo <code>Yhteyssaie</code>-olion, joka saa <code>Sailio</code>-olion
	 * @param sailio
	 */
	public Yhteyssaie(Sailio sailio) {
		this.sailio = sailio;
	}

	/**
	 * <code>Yhteyssaie</code> -saikeen <code>run()</code>-metodi,
	 * joka luo <code>Kasittelijasaie</code>-saikeen ja <code>Objektisaie</code>-saikeen.
	 */
	@Override
	public void run(){
			try {
				ServerSocket serveri = new ServerSocket(1111);
				ServerSocket serveri2 = new ServerSocket(1112);
				do {

						Socket s = serveri.accept();
						Socket s2 = serveri2.accept();
						Kasittelijasaie kasittelijasaie = new Kasittelijasaie(s, sailio);
						kasittelijasaie.start();
						Objektisaie objektisaie = new Objektisaie(sailio, s2);
						objektisaie.start();


				}while(true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
}
