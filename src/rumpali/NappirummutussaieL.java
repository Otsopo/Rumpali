package rumpali;
/**
 * <code>NappirummutussaieL</code> saie, joka kuuntelee vasenta nappia ja pyorittaa vasenta moottoria.
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */
public class NappirummutussaieL extends Thread {
	private Nappi nappi;
	private Moottorit moottorit;
	private volatile boolean valmis = false;

	/**
	 * luokan konstruktori joka asettaa napin ja moottorin.
	 * @param nappi
	 * @param moottorit
	 */
	public NappirummutussaieL(Nappi nappi, Moottorit moottorit){
		this.nappi = nappi;
		this.moottorit = moottorit;
	}

	/**
	 * kun saie kaynnistetaan <code>run</code> metodilla se kuuntelee vasenta nappia. Kun nappi on painettu, saie pyorittaa vasenta moottoria.
	 */
	@Override
	public void run(){
		while (!valmis){
			while (nappi.getLeft()){
				moottorit.vasen();
			}
		}

	}

	/**
	 * saikeen lopetus metodi <code>lopeta</code>.
	 */
	public void lopeta(){
		valmis = true;
	}



}
