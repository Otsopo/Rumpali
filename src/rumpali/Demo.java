
package rumpali;

import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

/**
 * Luokka <code>Demo</code> on robotin demo Behavior
 *
 * @author Lauri, Tiia, Otso.
 * @version 1.0
 */

public class Demo implements Behavior{

	/**
	 * Boolean, joka ilmaisee onko kayttaytyminen toiminnassa.
	 */

	private volatile boolean suppressed = false;
	/**
	 * Moottori olio, jota luokka kayttaa.
	 */
	private Moottorit moottorit;
	/**
	 * Sailio olio, jota luokka kayttaa.
	 */
	private Sailio sailio;
	/**
	 * Luo <code>Demo</code> olion ilmentyman, jonka moottoreiksi annetaan <code>moottorit>/code> ja sailioksi <code>sailio</code>
	 * @param moottorit
	 * 					Ilmentyman moottorit
	 * @param sailio
	 * 					Ilmentyman sailio
	 */

	public Demo(Moottorit moottorit, Sailio sailio) {
		this.moottorit = moottorit;
		this.sailio = sailio;
	}
	/**
	 * Kayttaytymisen toiminnan kaynnistaja. Palauttaa true, kun sailiosta haettu luku on 2.
	 * @return kaynnistyyko toiminta vai ei
	 */

	@Override
	public boolean takeControl() {
		if(sailio.getLuku()==2) {
			return true;
		}
		return false;

	}
	/**
	 * Kayttaytymisen sisalto on, etta moottorit toimivat maaratylla tavalla.
	 */

	@Override
	public void action() {
		suppressed = false;

		moottorit.oikea();
		moottorit.vasen();
		moottorit.molemmat();
		suppress();
	}
	/**
	 * Kayttaytymisen lopettaja
	 */

	@Override
	public void suppress() {
		suppressed = true;
	}


}
